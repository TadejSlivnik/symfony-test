# Symfony (2.7) Application Test

## Programs used

* PHP 7.1 - <https://www.php.net/>
* XAMPP for Apache, MySQL and phpMyAdmin - <https://www.apachefriends.org/index.html>
* Composer - <https://getcomposer.org/>
* Visual Studio Code - <https://code.visualstudio.com/>

## Important Folders

* Model/Entity - `src/AppBundle/Entity` 
* View - `app/Resources/views` 
* Controller - `src/AppBundle/Controller` 
* "Public" resources (e.g.: CSS, JS) - `web/` 
* App settings - `app/config/` 

## Setup - Reminders

### Creating Symfony Applications with Composer

This command creates a Symfony 2.7 application in a folder named "symfony-test" + installs Doctrine, Twig and everything else we need.

``` dos
composer create-project symfony/framework-standard-edition symfony-test "2.7.*"
```

### Configuring a Database

Configure your database information in ` `  ` app/config/parameters.yml `  ` ` .
Create your database using ` `  ` php app/console doctrine:database:create `  ` ` .

### Installing/Updating Symfony Packages (composer.json)

``` dos
composer install
```

``` dos
composer update
```

### Run Built-in Server

``` dos
php app/console server:run
```

### How to Generate Entities from an Existing Database

Create metadata files using the folowing command. AppBundle is the default bundle name. This command asks Doctrine to introspect the database and generate the XML metadata files under `src/AppBundle/Resources/config/doctrine` .

``` dos
php app/console doctrine:mapping:import --force AppBundle xml
```

Generate entity classes with annotation mappings under `src/AppBundle/Entity` :

``` dos
php app/console doctrine:mapping:convert annotation ./src
```

### Update your Database Schema

``` dos
php app/console doctrine:schema:update --force
```

### Password Encoding (Hashing)

``` yml
# app/config/security.yml
security:
    encoders:
        AppBundle\Entity\User:
            algorithm: bcrypt
    
    # ...
    
    providers:
        our_db_provider:
            entity:
                class: AppBundle:User
                property: username # variable name
                # if you're using multiple entity managers
                # manager_name: customer
    
    firewalls:
        main:
            pattern:    ^/
            http_basic: ~
            provider: our_db_provider # name doesnt mather but must match provider's name
    
    # ...
```

## Good Practices

### Controller

``` php
class ArticleController extends Controller
{
    /**
     * @Route(
     *     "/articles/{_locale}/{year}/{slug}.{_format}",
     *     defaults={"_format": "html"},
     *     requirements={
     *         "_locale": "en|fr",
     *         "_format": "html|rss",
     *         "year": "\d+"
     *     }
     * )
     * @Method({"GET", "HEAD"}}
     */
    public function showAction($_locale = "en", $year, $slug)
    { }
    
    /**
     * @Route(...)
     * @Method({"PUT"}}
     */
    public function showAction($_locale = "en", $year, $slug)
    { }
}
```

#### Errors

``` php
public function index()
{
    // retrieve the object from database
    $product = ...;
    if (!$product) {
        throw $this->createNotFoundException('The product does not exist');
    
        // the above is just a shortcut for:
        // throw new NotFoundHttpException('The product does not exist');
    }
    
    return $this->render(...);
}
```

``` php
// this exception ultimately generates a 500 status error
throw new \Exception('Something went wrong!');
```

### Twig

#### Linking to pages

``` twig
<a href="{{ path('welcome') }}">Home</a>
```

``` twig
{% for article in articles %}
    <a href="{{ path('article_show', {'slug': article.slug}) }}">
        {{ article.title }}
    </a>
{% endfor %}
```

Absolute URL:

``` twig
<a href="{{ url('welcome') }}">Home</a>
``` 

#### Linking to assets

``` twig
<img src="{{ asset('images/logo.png') }}" alt="Symfony!"/>
<link href="{{ asset('css/blog.css') }}" rel="stylesheet"/>
<script src="{{ asset('js/main.js') }}"></script>
```

Absolute URL:

``` twig
<img src="{{ absolute_url(asset('images/logo.png')) }}" alt="Symfony!"/>
```

### Flash Messages (not implemented)

``` php
public function update(Request $request)
{
    // ...
    
    if ($form->isSubmitted() && $form->isValid()) {
        // do some sort of processing
    
        $this->addFlash(
            'notice',
            'Your changes were saved!'
        );
        // $this->addFlash() is equivalent to $request->getSession()->getFlashBag()->add()
    
        return $this->redirectToRoute(...);
    }
    
    return $this->render(...);
}
```

``` twig
{# templates/base.html.twig #}

{# read and display just one flash message type #}
{% for message in app.flashes('notice') %}
    <div class="flash-notice">
        {{ message }}
    </div>
{% endfor %}

{# read and display several types of flash messages #}
{% for label, messages in app.flashes(['success', 'warning']) %}
    {% for message in messages %}
        <div class="flash-{{ label }}">
            {{ message }}
        </div>
    {% endfor %}
{% endfor %}

{# read and display all flash messages #}
{% for label, messages in app.flashes %}
    {% for message in messages %}
        <div class="flash-{{ label }}">
            {{ message }}
        </div>
    {% endfor %}
{% endfor %}
```

## Links

### PHP

* Documentation - <https://www.php.net>
* PDO Tutorial - <https://phpdelusions.net/pdo>

### MySQL

* Documentation - <https://dev.mysql.com/doc>
* Tutorial - <http://www.mysqltutorial.org>

### Doctrine

* Documentation - <https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/index.html>
* Cheat Sheet - <http://ormcheatsheet.com>

### Symfony

* Documentation - <https://symfony.com/doc/current/index.html#gsc.tab=0>
* Cookbook - <https://symfony.com/doc/2.6/cookbook/index.html>
* Tutorial - <https://symfonycasts.com/screencast/symfony>

