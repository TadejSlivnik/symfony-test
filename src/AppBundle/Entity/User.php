<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Users
 *
 * @ORM\Table(name="users")
 * @ORM\Entity
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="post_number", type="string", length=10)
     */
    private $postNumber = 'NULL';

    /**
     * @ORM\Column(name="password", type="string", length=64)
     * Password je lahko plain ali pa encoded! Iz forme se dobi plain, iz baze pa encoded.
     */
    private $password;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($_name)
    {
        $this->name = $_name;
    }

    public function getPostNumber()
    {
        return $this->postNumber;
    }

    public function setPostNumber($_post)
    {
        $this->postNumber = $_post;
    }

    // USER INTERFACE

    public function getUsername()
    {
        return $this->name;
    }

    public function getSalt()
    {
        return null; // bcrypt
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function eraseCredentials()
    { }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->name,
            $this->postNumber,
            $this->password,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->name,
            $this->postNumber,
            $this->password,
        ) = unserialize($serialized, ['allowed_classes' => false]);
    }
}
