<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("name", 'text', array("attr" => array("class" => "form-control")))
            ->add("postNumber", 'text', array("attr" => array("class" => "form-control")))
            ->add("password", 'password', array("attr" => array("class" => "form-control")))
            ->add("save", 'submit', array("label" => "Create", "attr" => array("class" => "button")));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }

    public function getName()
    {
        return 'user';
    }
}
