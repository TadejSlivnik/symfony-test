<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class SearchController extends Controller
{
    /**
     * Returns search form.
     */
    public function searchBarAction()
    {
        $form = $this->createFormBuilder(null)
            ->add('search', 'text')
            ->setAction($this->generateUrl("handleSearch"))
            ->getForm();

        return $this->render(
            'searchBar.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/handleSearch", name="handleSearch")
     * @Method({"POST"})
     * Extracts users "search" value and redirects to "homepage".
     */
    public function handleSearch(Request $request)
    {
        $searchvalue = $request->request->get("form")["search"];
        return $this->redirectToRoute("homepage", array("postNumber" => $searchvalue));
    }

    /**
     * Sprogramiraj algoritem, ki vzame vse artikle na zalogi in najde kombinacijo artiklov, 
     * ki ima največ unikatnih artiklov, ki imajo skupaj ceno pod 200. 
     * Za primer si oglej knapsack problem.
     * 
     * Zalogo sem jaz tretiral kot: vse kar lahko dobim iz baze.
     * 
     * Ta "algoritem" deluje le za ta primer -> NAJVEČ NEPONAVLJAJOČIH artiklov, ki imajo skupno ceno < $maxCena
     */
    // public function artikli200()
    // {
    //     $maxCena = 200;
    //     $repository = $this->getDoctrine()->getRepository("Artikel::class");

    //     // iz baze vzamemo vse artikle (naj ne bi bili podvojeni), ki imajo ceno manjšo od $maxCena, in jih sortiramo (nižje cene naprej)
    //     $query = $repository->createQueryBuilder('artikel')
    //         ->where('artikel.price < :maxCena')
    //         ->setParameter('maxCena', $maxCena)
    //         ->orderBy('artikel.price', 'ASC') // nižje cene naprej
    //         ->getQuery();
    //     $products = $query->getResult(); // to verjetno vrne array()?

    //     $sum = 0;
    //     $n = 0;
    //     for ($i = 0; $i < count($products); $i++) {
    //         $sum += $products[$n]->price;
    //         // break, ce je sum vecji (ali enak) kakor cena
    //         if ($sum >= $maxCena) {
    //             break;
    //         }
    //         $n++;
    //     }
        
    //     //     artikli od ničtega do ($n-1)-tega
    //     // oz: artikli od prvega do ($n)-tega
    //     return array_slice($products, 0, $n);
    // }
}
