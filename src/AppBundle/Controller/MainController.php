<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class MainController extends Controller
{
    /**
     * @Route("/add", name="add")
     * @Method({"GET", "POST"})
     */
    public function add(Request $request)
    {
        $newUser = new User();
        // če to dvoje spodaj ne dam na prazen string, v input-u piše NULL... zakaj?
        $newUser->setName("");
        $newUser->setPostNumber("");

        $form = $this->createForm(new UserType, $newUser);
        $form->handleRequest($request); // ali je POST

        if ($form->isSubmitted() && $form->isValid()) {
            $newUser = $form->getData();

            // encode password and set it to newUser
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($newUser, $newUser->getPassword());
            $newUser->setPassword($encoded);

            // vnos v bazo
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newUser);
            $entityManager->flush();

            return $this->redirectToRoute("homepage");
        }

        return $this->render(
            "formPage.html.twig",
            array("form" => $form->createView(), "formName" => "Add New User")
        );
    }

    /**
     * @Route("/delete/{id}", name="delete")
     * @Method({"DELETE"})
     */
    public function delete($id = null, Request $request)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        // this should never happen
        if (!$user) {
            throw $this->createNotFoundException("The user with id " . $id . " does not exists!");
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();

        return $this->redirectToRoute("homepage");
    }

    /**
     * @Route("/login", name="login")
     * @Method({"GET", "POST"})
     * Creates a login form. If its a POST event, it checks if the (user)name exists in the database, if it does it checks the password. Also returns message.
     * NOT A GOOD WAY TO LOGIN USERS: database can contain users with the same name -> controller returns the first user with that name and checks the password.
     */
    public function login(Request $request)
    {
        $loginUser = new User();
        $loginUser->setName("");

        $form = $this->createFormBuilder($loginUser)
            ->add("name", 'text', array("attr" => array("class" => "form-control")))
            ->add("password", 'text', array("attr" => array("class" => "form-control")))
            ->add("save", 'submit', array("label" => "Login", "attr" => array("class" => "button")))
            ->getForm();

        $form->handleRequest($request);

        $renderArray = array("form" => $form->createView(), "formName" => "Login");
        if ($form->isSubmitted() && $form->isValid()) {
            $loginUser = $form->getData();
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(array("name" => $loginUser->getName()));

            if ($user == null) {
                $renderArray["msg"] = "User does not exist...";
            } else {
                $encoder = $this->container->get('security.password_encoder');
                $matching = $encoder->isPasswordValid($user, $loginUser->getPassword());

                if ($matching) {
                    $renderArray["msg"] = "CONGRATULATIONS, YOU ARE 'LOGGED IN'! Sadly, this app does not have a page for logged-in users and does not support sessions.";
                } else {
                    $renderArray["msg"] = "User exists but password is incorrect...";
                }
            }
        }

        return $this->render(
            "formPage.html.twig",
            $renderArray
        );
    }

    /**
     * @Route("/{postNumber}", name="homepage")
     * @Method({"GET"})
     * Uses doctrine to get values from database and renders "main.html.twig". This function needs to be last otherwise it overwrites other links /{wildcard}.
     */
    public function main($postNumber = null)
    {
        $parameters = array();
        if ($postNumber == null) {
            $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        } else {
            $users = $this->getDoctrine()->getRepository(User::class)->findBy(array("postNumber" => $postNumber));
            $parameters["goHome"] = true;
        }

        $parameters["users"] = $users;

        return $this->render(
            "main.html.twig",
            $parameters
        );
    }
}
